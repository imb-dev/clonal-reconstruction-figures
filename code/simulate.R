###############################################################################
# simulate.R
#
# This file contains the simulation routine for clonal time courses as well as
# the function to superimpose integration sites to these time courses.
###############################################################################

#' Runs a clonal simulation and augments it by applying integration sites to
#' the clones.
#'
#' @export
#' @param ro_compartments The number of compartments to simulate.
#' @param tps The time points to return.
#' @param nr_clones The initial number of clones.
#' @param target_vcn The mean number of integration sites that are assigned to
#'                   each clone. The number of integration sites per clone is
#'                   Poisson distributed.
#' @param clonal_variability Variation that is applied to all integration sites
#'                           of a clone equally.
#' @param measurement_noise Variation that is applied to all integration sites
#'                          regardless of which clone they belong to.
#' @param simulate_clones_params A named list of parameters that will be passed
#'                               to the clonal simulation. The clonal simulation
#'                               is currently performed using
#'                               simulate_clones_competition.
#' @param use_amplification Whether amplification factors should be used when
#'                          handling integration sites. If `FALSE`, the
#'                          amplification factors for each clone will be 1.
#' @return A named list containing:
#'         - params: the parameters passed to this function
#'         - amplification_rates: the amplification rates if the integration
#'                                sites
#'         - mapping: a matrix that describes which integration site was
#'                    assigned to which clone
#'         - clone_counts: the raw clone counts, without application of
#'                         clonal_variability or measurement_noise
#'         - clone_readouts: the clone counts, after application of
#'                           clonal_variability
#'         - is_counts: the number of integration site reads after mapping the
#'                      ISs to clones but before application of
#'                      measurement_noise
#'         - is_readouts: the reads of integration sites after application of
#'                        measurement_noise
simulate <- function(ro_compartments = 1, # number of read out compartments
                     tps,
                     nr_clones,
                     target_vcn,
                     clonal_variability,
                     measurement_noise,
                     simulate_clones_params,
                     use_amplification) {
  if (is.null(simulate_clones_params)) {
    simulate_clones_params <- list(
      nr_clones = nr_clones,
      tps = tps,
      initdist = list(type = "gamma", gamma_shape = 1, gamma_scale = 30),
      prolif = list(
        type = "nDistributedFixed",
        n_distributed_mean = 0.1,
        n_distributed_sigma = 0.0,
        with_lf = TRUE,
        cc = 2000,
        fixed_values = c()
      ),
      diff = list(
        type = "nDistributedFixed",
        n_distributed_mean = 0.03,
        n_distributed_sigma = 0.002,
        with_lf = FALSE, cc = 1000,
        fixed_values = c()
      )
    )
  }

  params <- list(
    ro_compartments = ro_compartments,
    tps = tps,
    nr_clones = nr_clones,
    target_vcn = target_vcn,
    clonal_variability = clonal_variability,
    measurement_noise = measurement_noise,
    simulate_clones_params = simulate_clones_params
  )

  data <- list(
    params = params,
    amplification_rates = c(),
    mapping = matrix(),
    clone_counts = matrix(),
    clone_readouts = matrix(),
    is_counts = matrix(),
    is_readouts = matrix()
  )

  cd <- do.call("simulate_clones_competition", simulate_clones_params)
  cd <- cd[, rep(seq_len(ncol(cd)), each = ro_compartments), drop = FALSE]
  data$clone_counts <- cd

  af <- stats::rnorm(length(cd), mean = 1, sd = clonal_variability)
  cd <- cd * af
  cd[cd < 0] <- 0
  data$clone_readouts <- cd

  # IS (list of amplification rates and number of BCs per clone)
  bcs <- lapply(seq_len(nrow(data$clone_readouts)), function(e) {
    # amplification rate for each clone
    if (target_vcn == "oneFixed") {
      return(1.0)
    }
    if (use_amplification) {
      stats::runif(
        n = as.integer(max(1, stats::rpois(1, lambda = target_vcn))),
        min = 0.2, max = 1.0
      )
    } else {
      stats::runif(
        n = as.integer(max(1, stats::rpois(1, lambda = target_vcn))),
        min = 1.0, max = 1.0
      )
    }
  })

  data$amplification_rates <- bcs

  data$mapping <- matrix(ncol = 2, nrow = length(unlist(bcs)), data = NA)
  colnames(data$mapping) <- c("Clone", "IS")
  bcnr <- 1
  for (cl in seq_len(length(bcs))) {
    for (bc in bcs[[cl]]) {
      data$mapping[bcnr, ] <- c(cl, bcnr)
      bcnr <- bcnr + 1
    }
  }

  # barcode data (the readout, no data yet)
  bd <- matrix(
    nrow = length(do.call("c", bcs)),
    ncol = ncol(cd),
    data = NA,
    dimnames = list(seq_len(length(do.call("c", bcs))), seq_len(ncol(cd)))
  )

  for (i in seq_len(ncol(cd))) { # for all time points
    bd[, i] <- as.integer(
      do.call("c", bcs) *
        as.numeric(rep(cd[, i], unlist(lapply(bcs, length)))))
  }
  colnames(bd) <- colnames(cd)

  data$is_counts <- bd

  bd <- matrix(
    stats::rnorm(n = length(bd),
                 mean = as.numeric(bd),
                 sd = as.numeric(bd) * measurement_noise),
    nrow = nrow(bd),
    ncol = ncol(bd),
    dimnames = list(rownames(bd), colnames(bd))
  )

  bd[bd < 0] <- 0 # might happen due to rnorm earlier
  data$is_readouts <- bd

  # see plot.timeseries
  class(data$is_readouts) <-
    class(data$clone_readouts) <-
    class(data$is_counts) <-
    class(data$clone_counts) <- c(class(data$is_readouts), "timeseries")

  return(data)
}

# Function for the calculation of the change in the rates
diff_rates <- function(last_state, nrct, prolif, diff) {
  old_size <- rep(sum(last_state), nrct)

  # Calculates the rates, depending on the method for proliferation
  switch(prolif$type,
    "nDistributed" = {
      pval <- stats::rnorm(n = nrct,
                           mean = prolif$n_distributed_mean,
                           sd = prolif$n_distributed_sigma)
      pval[pval < 0] <- 0
    },
    "eDistributed" = {
      pval <- stats::runif(n = nrct)
    },
    "nDistributedFixed" = {
      pval <- prolif$fixed_values
    },
    {
      stop("Unsupported proliferation distribution.")
    }
  )

  # Distinguish with / without logistic function for proliferation
  if (prolif$with_lf == TRUE) {
    dumping <- 1 - old_size / prolif$cc
    dumping[which(dumping > 1)] <- 1
    dumping[which(dumping < 0)] <- 0
    pval <- pval * dumping
  }

  # Part for diff calculation
  dval <- rep(0, nrct) # The calculated difference to the last value

  # Calculates the rates, depending on the method for diff
  switch(diff$type,
    "nDistributed" = {
      dval <- stats::rnorm(n = nrct,
                           mean = diff$n_distributed_mean,
                           sd = diff$n_distributed_sigma)
      dval[dval < 0] <- 0
    },
    "eDistributed" = {
      dval <- stats::runif(n = nrct)
    },
    "nDistributedFixed" = {
      dval <- diff$fixed_values
    },
    {
      stop("Unsupported differentiation distribution.")
    }
  )

  # Distinguish with / without logistic function for diff
  if (diff$with_lf == TRUE) {
    dumping <- old_size / diff$cc
    dumping[which(dumping > 1)] <- 1
    dumping[which(dumping < 0)] <- 0
    dval <- dval * dumping
  }

  out <- list(
    p = pval,
    d = dval
  )

  return(out)
}

#' A more complex model for clonal hematopoiesis. Incorporates different
#' choices for the initial distribution of clones, the proliferation and
#' differentiation rates.
#'
#' @export
#' @param nr_clones The initial number of clones
#' @param tps The time points to return.
#' @param initdist Describes the inital distribution of clones. Possible
#'                 values are: "equal", "defined", "uniform", "gamma",
#'                 "pareto", "powerlaw", and "nbinom". See examples.
#' @param prolif Describes the distribution of proliferation rates.
#'               Possible values are: "nDistributed", "eDistributed",
#'               "nDistributedFixed". See example.
#' @param diff Describes the distribution of differentiation rates.
#'             Possible values are: "nDistributed", "eDistributed",
#'             "nDistributedFixed". See example.
#' @param break_at Allows a clone to expose different behaviour from a
#'                 certain time point on. This is expressed as a named
#'                 list with entries "cell", "tp", "p", and "d". The
#'                 value vectors need to be of the same length and are
#'                 position-matched. "cell" describes the indices of the
#'                 cells for which the proliferation "p" and
#'                 differentiation "d" will be altered from time point
#'                 "tp" on. See example.
#' @param ... Additional agruments will be discarded.
#' @example
#' # Create a time course
#' simulate_clones_competition(
#'   # 100 slones
#'   nr_clones = 100,
#'   # Simulate time point 1..100
#'   tps = seq(1, 100),
#'   # Describes an initial distribution, where all cells are
#'   # initialized with the same size of 50
#'   initdist = list(
#'     type = "equal",
#'     equal_equc = 50),
#'   # Describes the distribution of the proliferation values.
#'   # With "nDistributedFixed", the base proliferation rates
#'   # are drawn at the beginning from a normal distribution
#'   # N(0.3, 0.1^2). The effective proliferation rates are
#'   # calculated in each time point depending on the size of
#'   # the clone and the carrying capacity "cc"
#'   prolif = list(
#'     type = "nDistributedFixed",
#'     n_distributed_mean = 0.3,
#'     n_distributed_sigma = 0.1,
#'     with_lf = TRUE,
#'     cc = 2500),
#'   # Same as the proliferation, but for N(0.2, 0.05^2) and
#'   # without a limitation by the carrying capacity.
#'   diff = list(
#'     type = "nDistributedFixed",
#'     n_distributed_mean = 0.2,
#'     n_distributed_sigma = 0.05,
#'     with_lf = FALSE),
#'   # At time point 5, cell 2 exposes altered behaviour. Its
#'   # proliferation is set to 0.8 and its differentiation to
#'   # 0.2. Note, that this time, the values are not drawn from
#'   # a normal distribution, but set as they are.
#'   break_at = list(
#'     "cell" = 2,
#'     tp = 5,
#'     p = 0.8,
#'     d = 0.2))
#' @return A more complex clonal time course.
simulate_clones_competition <- function(nr_clones,
                                        tps,
                                        initdist,
                                        prolif,
                                        diff,
                                        break_at = NULL,
                                        ... # to ignore excess arguments
) {
  # Check for argument types and lengths
  stopifnot(is.numeric(nr_clones) && length(nr_clones) == 1)
  stopifnot(is.numeric(tps))
  stopifnot(!is.null(initdist) && is.list(initdist))
  stopifnot(!is.null(prolif) && is.list(prolif))
  stopifnot(!is.null(diff) && is.list(diff))
  stopifnot(is.null(break_at) || is.list(break_at))
  stopifnot(is.null(break_at) || length(break_at$cell) == length(break_at$tp))
  stopifnot(is.null(break_at) || length(break_at$cell) == length(break_at$p))
  stopifnot(is.null(break_at) || length(break_at$cell) == length(break_at$d))

  nr_clones <- as.integer(nr_clones)
  tps <- as.integer(tps)

  # configuration for the timepoints used
  nrct <- nr_clones
  ts_stop <- max(tps)
  nrcomp <- 1

  cc_matrix <- rep(x = 0, nrct)

  # initiate.cc_matrix
  switch(initdist$type,
    "equal" = {
      cc_matrix <- rep(initdist$equal_equc, length(cc_matrix))
    },
    "fixed" = {
      cc_matrix <- initdist$fixed_values
    },
    "uniform" = {
      cc_matrix <- as.integer(stats::runif(min = initdist$uniform_minc,
                                           max = initdist$uniform_maxc,
                                           length(cc_matrix)))
    },
    "gamma" = {
      cc_matrix <- as.integer(stats::rgamma(n = length(cc_matrix),
                                            shape = initdist$gamma_shape,
                                            scale = initdist$gamma_scale))
    },
    "pareto" = {
      cc_matrix <- as.integer(rmutil::rpareto(n = length(cc_matrix),
                                              m = initdist$pareto_m,
                                              s = initdist$pareto_s))
    },
    "powerlaw" = {
      cc_matrix <- as.integer(poweRlaw::rpldis(n = length(cc_matrix),
                                               xmin = initdist$powerlaw_xmin,
                                               alpha = initdist$powerlaw_alpha))
    },
    "nbinom" = {
      cc_matrix <- stats::rnbinom(n = length(cc_matrix),
                                  size = initdist$nbinom_size,
                                  mu = initdist$nbinom_mu)
    },
    {
      stop("Unsupported initial distribution.")
    }
  )

  # Here we initialize the matrices for the nDistributionFixed configuration
  if (is.numeric(prolif$fixed_values)) {
    # holidays :)
  } else if (prolif$type == "nDistributedFixed") {
    prolif$fixed_values <-
      stats::rnorm(n = nrct,
                   mean = prolif$n_distributed_mean,
                   sd = prolif$n_distributed_sigma)
    prolif$fixed_values[which(prolif$fixed_values < 0)] <-
      prolif$n_distributed_mean
  }

  if (is.numeric(diff$fixed_values)) {
    # nothing to do at all
  } else if (diff$type == "nDistributedFixed") {
    diff$fixed_values <-
      stats::rnorm(n = nrct,
                   mean = diff$n_distributed_mean,
                   sd = diff$n_distributed_sigma)
    diff$fixed_values[which(diff$fixed_values < 0)] <-
      diff$n_distributed_mean
  }

  # Initializing carying capacity for the compartments
  if (nrcomp > 1) {
    prolif$cc <- rep(prolif$cc, each = nrct / nrcomp)
    diff$cc <- rep(diff$cc, each = nrct / nrcomp)
  }

  names(cc_matrix) <- lapply(seq(seq_len(length(cc_matrix))),
                             function(x) paste("c", x))

  ftc <- matrix(
    NA_integer_,
    nrow = length(tps),
    ncol = nrct
  )
  ftc[1, ] <- cc_matrix

  t <- 0
  while (TRUE) {
    # If a cell breaks, we give it a new diff and prolif rate
    if (!is.null(break_at) && t %in% break_at$tp) {
      if (prolif$type != "nDistributedFixed" || diff$type != "nDistributedFixed")
        stop("Breaking cells currently only supported for prolif/diff-type nDistributedFixed")
      idxs <- which(break_at$tp == t)
      cell_idxs <- break_at$cell[idxs]
      prolif$fixed_values[cell_idxs] <- break_at$p[idxs]
      diff$fixed_values[cell_idxs] <- break_at$d[idxs]
    }
    # Calculating the diff rates for this step
    pd <- diff_rates(cc_matrix, nrct, prolif, diff)
    pd$p[pd$p > 1] <- 1
    pd$d[pd$d > 1] <- 1

    # Calculating the absolute values for the considered time point, based on
    # the proliferation rates p and diff rates d
    cc_matrix <- cc_matrix -
      stats::rbinom(n = length(cc_matrix), size = cc_matrix, prob = pd$d) +
      stats::rbinom(n = length(cc_matrix), size = cc_matrix, prob = pd$p)

    # If the number overcomes the maximum integer, set it to the max
    cc_matrix[which(is.na(cc_matrix))] <- .Machine$double.max.exp

    t <- t + 1
    if (t %in% tps) {
      ftc[which(t == tps), ] <- cc_matrix
    }

    if (t == ts_stop) {
      break
    }
  }

  ftc <- t(ftc) # remove unused rows and transpose
  colnames(ftc) <- tps
  return(ftc)
}
